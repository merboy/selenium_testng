package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.navigation;
import common.setup;

public class landing_page {
	
	setup setup = new setup();
	Properties properties = new Properties();
	navigation Navigation;
	
	
	@BeforeTest
	public void preTest() throws FileNotFoundException, IOException {
		properties.load(new FileReader(new File("src/properties/user.properties")));
		setup.startBrowser(properties.getProperty("chrome"), properties.getProperty("url"));
	}
	
	@BeforeClass 
	public void beforeClass() throws FileNotFoundException, IOException {
		Navigation = new navigation();
	}
	
	// validates the texts in the categories section of the landing page
	@Test(priority = 0)
	public void landing_categories() {
		Navigation = new navigation();
		String[] expected_values = properties.get("cat_texts").toString().split("#");
		assert(Navigation.compareList(properties.getProperty("categories"), expected_values, "categories" ));
	}
	
	@AfterTest
	public void postTest() {
		setup.closeBrowser();
	}

}
