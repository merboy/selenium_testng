package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

import common.navigation;

public class create_account {
	
	Properties properties = new Properties();
	
	
	@Test(priority = 0)
	public void register_page() throws FileNotFoundException, IOException{
		navigation navigation = new navigation();
		properties.load(new FileReader(new File("src/properties/user.properties")));
		navigation.findElement("xpath", properties.getProperty("register"), "Sign Up button").click();
		navigation.findElement("xpath", properties.getProperty("phone"), "Phone Number").sendKeys(properties.getProperty("phonenumber"));
		navigation.findElement("xpath", properties.getProperty("name"), "Name").sendKeys(properties.getProperty("fullname"));
		
	}

}
