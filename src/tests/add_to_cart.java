package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.navigation;



public class add_to_cart {

	Properties properties = new Properties();
	navigation Navigation;
	
	@BeforeClass 
	public void beforeClass() throws FileNotFoundException, IOException {
		properties.load(new FileReader(new File("src/properties/user.properties")));
		Navigation = new navigation();
	}
	
	@Test(priority = 0)
	public void search_item() {
		Navigation.findElement("xpath", properties.getProperty("search_field"), "Search field").sendKeys(properties.getProperty("search_item"));
		Navigation.findElement("xpath", properties.getProperty("search_btn"), "Search button").click();
		
	}
	@Test(priority = 1)
	public void random_add_cart() {
		Navigation.findElement("xpath", properties.getProperty("products"), "Products").click();
		Navigation.findElement("xpath", properties.getProperty("add_cart"), "Products").click();
		
	}
}

