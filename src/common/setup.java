package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class setup {
	public static WebDriver driver;	
	public void startBrowser(String browser, String url) {
		System.setProperty("webdriver.chrome.driver", browser);
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	
	public void closeBrowser() {
		driver.close();
	}
	

}
