package common;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class navigation {
	
	private static final long FIND_ELEMENT_TIMEOUT = 10L;
	WebDriver driver = setup.driver;
	
	public WebElement findElement(String locType, String locator, String elem) {
		WebElement element = null;

		 try
	        {
	        	if(locType.equalsIgnoreCase("id")){
	        		return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                    .visibilityOfElementLocated(By.id(locator)));
	            
	        	} else if (locType.equalsIgnoreCase("name")){        		
	        		return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                    .visibilityOfElementLocated(By.name(locator)));
	            
	            } else if (locType.equalsIgnoreCase("linktext")){        		
	            	return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                        .visibilityOfElementLocated(By.linkText(locator)));
	                
	            } else if (locType.equalsIgnoreCase("partiallinktext")){        		
	            	return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                        .visibilityOfElementLocated(By.partialLinkText(locator)));
	                
	            } else if (locType.equalsIgnoreCase("css")){        		
	            	return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                        .visibilityOfElementLocated(By.cssSelector(locator)));
	                
	            } else if (locType.equalsIgnoreCase("class")){        		
	            	return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                        .visibilityOfElementLocated(By.className(locator)));
	            } else{        		
	            	return (WebElement) new WebDriverWait(driver,FIND_ELEMENT_TIMEOUT).until(ExpectedConditions
	                    .visibilityOfElementLocated(By.xpath(locator)));
	            }
	        }
	        catch (Exception e)
	        {
	            System.out.println(elem+" is not found");
	            return element;
	            
	        	}
	}

	
	public Boolean assertElement(String locType, String locator, String elem) {
		return findElement(locType, locator, elem).isDisplayed(); 	
			
	}
	
	
	

	public Boolean compareList(String locator, String[] expected_values, String elem) {
		List <WebElement> items =  driver.findElements(By.xpath(locator));	
		//ArrayList<String> a = new ArrayList<String>();
		for (int count = 0; count < items.size(); count++) {
			if(expected_values[count].equals(items.get(count).getText() ) ){
				System.out.println(items.get(count).getText());
			}else {
				System.out.println("Expected :"+expected_values[count]);
				System.out.println("Actual :"+items.get(count).getText());
				return false;
			}
		} 
		return true;
		
	}
}
